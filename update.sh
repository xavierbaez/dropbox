#!/bin/bash
set -e

pkgver=$(curl -I "https://www.dropbox.com/download?plat=lnx.x86_64" | grep -iP '^Location:' | grep -Po '\d+\.4\.\d+(?=\.tar\.gz)')
sed "s/^pkgver=.*/pkgver=$pkgver/" -i PKGBUILD
echo $pkgver
DISTRO=$(cat /etc/*release* | grep -Ei 'DISTRIB_ID' | head -n 1 | awk -F '=' '{print $2}')  
echo $DISTRO
if [ DISTRO="ManjaroLinux" ] || [ DISTRO="ManjaroLinux" ]; then 
    echo "Arch"
    #Arch Linux
    updpkgsums
    makepkg --printsrcinfo > .SRCINFO
else
    #Download 64-bit
    FOLDER=/opt/dropbox/
    for i in $(ls -1 "$FOLDER")
       do rm -Rvf  "$FOLDER$i"
    done
    rm -Rvf .dropbox-dist/dropbox-lnx.*
    wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -
    cp -pvdfR .dropbox-dist/dropbox-lnx.*/* "$FOLDER"
fi
